package ua.ck.geekhub;

import ua.ck.geekhub.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HelloController {

	@Autowired
	UserService userService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String printWelcome(ModelMap model) {
		model.addAttribute("message", "Hello world!");
		return "redirect:users";
	}

	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public String user(ModelMap model) {
		model.addAttribute("users", userService.getUsers());
		return "hello";
	}

	@RequestMapping(value = "/users", method = RequestMethod.POST)
	public String createUser(
			@RequestParam String firstName,
	        @RequestParam String lastName,
	        @RequestParam String email
	) {
		userService.createUser(firstName, lastName, email);
        userService.getUser(12);
        userService.deleteUser(new User());
       //userService.getUserH(12)
		return "redirect:users";
	}
}