<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<body>
	<h1>Hibernate Example</h1>
	<form action="${pageContext.request.contextPath}/users" method="post">
		<input name="firstName" placeholder="First Name">
		<input name="lastName" placeholder="Last Name">
		<input name="email" type="email" placeholder="Email">

		<input type="submit" value="Submit">
	</form>

	<table border="1">
		<tr>
			<td>ID</td>
			<td>FIRST NAME</td>
			<td>LAST NAME</td>
			<td>EMAIL</td>
			<td>GROUP</td>
		</tr>

		<c:forEach var="user" items="${users}">
		<tr>
			<td>${user.id}</td>
			<td>${user.firstName}</td>
			<td>${user.lastName}</td>
			<td>${user.email}</td>
			<td>${user.group.name}</td>
		</tr>
		</c:forEach>

	</table>
</body>
</html>